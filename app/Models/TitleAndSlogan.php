<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TitleAndSlogan extends Model
{
    use HasFactory;

    protected $table = 'title_and_slogans';

    protected $fillable = [
        'title',
        'slogan'
    ];
}
