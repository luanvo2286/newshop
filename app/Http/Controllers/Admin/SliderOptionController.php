<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SliderRequest;
use App\Models\Slider;
use Exception;
use Illuminate\Http\Request;


class SliderOptionController extends Controller
{
    public function sliderAdd(){

        return view('admin.slideradd', ['tag'=>'slider-option']);
    }

    public function sliderDelete($id){
        return response()->json(['data'=>$id]);
    }

    public function createSlider(SliderRequest $request){
       $data = [
        'title'=>$request->input('title'),
        'image'=>$_FILES['image']['name']
       ];

       try{
            Slider::create($data);
           
            $this->saveFile($_FILES, 'upload/');
            return redirect()->back()->with('success', 'Add Title And Image of Slider Successfully!');
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    public function sliderList(){
        $data = Slider::all()->toArray();
     
        return view('admin.sliderlist', ['data'=>$data]);
    }

    public function saveFile($FILE, $target_dir){
       
            if(!isset($FILE['image'])){
                //dd('error1');
                return redirect()->back()->withErrors('No file!!');
            }
            if ($_FILES["image"]['error'] != 0){
                //dd('error2');
                return redirect()->back()->withErrors('Upload File Error!');
            }
            $toDir = $target_dir.$FILE['image']['name'];
            move_uploaded_file($FILE['image']['tmp_name'], $toDir);
    }
}
