<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ChangePwdRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePwdController extends Controller
{
    public function index(){
        return view('admin.admin.changepassword');

    }


    public function change(ChangePwdRequest $request){
        
        $truePwd = Hash::check($request->oldpwd, Auth::user()->password);
        
        if($truePwd){
            $dataChange = [
                'password'=> bcrypt($request->newpwd),
            ];
    
            User::where('id', Auth::id())->update($dataChange);

            return redirect()->back()->with('changePass', 'Change pass successfull!');
        }

        return redirect()->back()->withErrors('Change Pass Faied!');
        
    }
}
