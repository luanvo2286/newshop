<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CopyRightRequest;
use App\Http\Requests\Admin\SocialRequest;
use App\Http\Requests\Admin\TitleAndSloganRequest;
use App\Models\CopyRight;
use App\Models\Social;
use App\Models\TitleAndSlogan;
use Illuminate\Http\Request;


class SiteOptionController extends Controller
{
    public function titleAndSlogan(){
        return view('admin.titleslogan', ['tag'=>'site-option']);
    }

    public function createTitleAndSlogan(TitleAndSloganRequest $request){
        $data = [
            'title'=>$request->title,
            'slogan'=>$request->slogan,
        ];

        if(empty(TitleAndSlogan::all()->toArray())){
            TitleAndSlogan::create($data);
            return redirect()->back()->with('success', 'Add Title And Slogan Successfully!');
        }else{
            TitleAndSlogan::where('id', 1)->update($data);
            return redirect()->back()->with('success', 'Add Title And Slogan Successfully!');
        }
        
    }

    public function social(){
        return view('admin.social', ['tag'=>'site-option']);
    }

    public function createSocial(SocialRequest $request){
        //dd(request());
        $data = [
            'facebook'=>$request->facebook,
            'twitter'=>$request->twitter,
            'linkedln'=>$request->linkedin,
            'google'=>$request->googleplus,
        ];
        //dd(Social::all());
        if(empty(Social::all()->toArray())){
            //dd('yyy');
            Social::create($data);
            return redirect()->back()->with('success', 'Add Social Successfully!');
        }else{
            Social::where('id', 1)->update($data);
            return redirect()->back()->with('success', 'Add Social Successfully!');
        }
        
    }

    public function copyRight(){

        return view('admin.copyright', ['tag'=>'site-option']);
    }

    public function createCopyRight(CopyRightRequest $request){

        $data = [
            'copyright'=>$request->copyright,
        ];
        //dd($data);
        if(empty(CopyRight::all()->toArray())){
            CopyRight::create($data);
            return redirect()->back()->with('success', 'Add CopyRight Successfully!');
        }else{
            CopyRight::where('id', 1)->update($data);
            return redirect()->back()->with('success', 'Add CopyRight Successfully!');
        }
    }
}
