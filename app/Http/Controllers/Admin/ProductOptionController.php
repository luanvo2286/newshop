<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductOptionController extends Controller
{
    public function productAdd(){

        return view('admin.productAdd');
    }

    public function productList(){

        return view('admin.productlist');
    }
}
