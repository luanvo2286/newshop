<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryOptionController extends Controller
{
    public function catAdd(){

        return view('admin.catadd');
    }
    
    public function catList(){

        return view('admin.catlist');
    }
}
