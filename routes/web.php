<?php

use App\Http\Controllers\Admin\CatAddController;
use App\Http\Controllers\Admin\CategoryOptionController;
use App\Http\Controllers\Admin\ChangePwdController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\ProductOptionController;
use App\Http\Controllers\Admin\SiteOptionController;
use App\Http\Controllers\Admin\SliderOptionController;
use Illuminate\Support\Facades\Auth;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//admin


Route::prefix('manager/admin')->group(function(){

    Route::get('/login', [LoginController::class, 'index']
    )->name('admin.login');
    Route::post('/login', [LoginController::class, 'validateLogin']
    );

    Route::middleware('admin')->group(function(){

        Route::get('/', function(){
            return view('admin.index', ['tag'=>'site-option']);
        })->name('admin.home');
        
        Route::get('/cat-add', [CategoryOptionController::class, 'catAdd'])->name('admin.catadd');
        Route::get('/cat-list', [CategoryOptionController::class, 'catList'])->name('admin.catlist');
    
        Route::get('/change-password', [ChangePwdController::class, 'index'])->name('admin.changepassword');
        Route::post('/change-password', [ChangePwdController::class, 'change']);

        Route::get('/copyright', [SiteOptionController::class, 'copyRight'])->name('admin.copyright');
        Route::post('/copyright', [SiteOptionController::class, 'createCopyRight']);

        Route::get('/social', [SiteOptionController::class, 'social'])->name('admin.social');
        Route::post('/social', [SiteOptionController::class, 'createSocial']);

        Route::get('/title-slogan', [SiteOptionController::class, 'titleAndSlogan'])->name('admin.titleslogan');
        Route::post('/title-slogan', [SiteOptionController::class, 'createTitleAndSlogan']);

        Route::get('/inbox', function(){
            return view('admin.inbox');
        })->name('admin.inbox');

        Route::get('/product-add', [ProductOptionController::class, 'productAdd'])->name('admin.productadd');
        Route::get('/product-list', [ProductOptionController::class, 'productList'])->name('admin.productlist');
    
        Route::get('/slider-add', [SliderOptionController::class, 'sliderAdd'])->name('admin.slideradd');
        ROute::get('/slider-delete/{id}', [SliderOptionController::class, 'sliderDelete']);
        Route::post('/slider-add', [SliderOptionController::class, 'createSlider']);
        Route::get('/slider-list', [SliderOptionController::class, 'sliderlist'])->name('admin.sliderlist');
    
    
        Route::get('/logout', function(){
            Auth::logout();
     
            request()->session()->invalidate();
         
            request()->session()->regenerateToken();
         
            return redirect()->route('admin.login');
        })->name('admin.logout');
    });
    

});





//frontend

Route::prefix('custom/frontend')->group(function(){

    Route::get('/', function(){
        return view('frontend.index');
    })->name('frontend.home');
    
    Route::get('/contact', function(){
        return view('frontend.contact');
    })->name('frontend.contact');
    
    Route::get('/cart', function(){
        return view('frontend.cart');
    })->name('frontend.cart');
    
    Route::get('/login', function(){
        return view('frontend.login');
    })->name('frontend.login');
    
    Route::get('/preview', function(){
        return view('frontend.preview');
    })->name('frontend.preview');
    
    Route::get('/product-by-cat', function(){
        return view('frontend.productbycat');
    })->name('frontend.productbycat');
    
    Route::get('/products', function(){
        return view('frontend.products');
    })->name('frontend.products');
    
    Route::get('/topbrands', function(){
        return view('frontend.topbrands');
    })->name('frontend.topbrands');
});


