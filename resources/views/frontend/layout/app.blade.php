<!DOCTYPE HTML>
<head>
<title>Store Website</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="{{asset('frontend/css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{asset('frontend/css/menu.css')}}" rel="stylesheet" type="text/css" media="all"/>
<script src="{{asset('frontend/js/jquerymain.js')}}"></script>
<script src="{{asset('frontend/js/script.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('frontend/js/jquery-1.7.2.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('frontend/js/nav.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/js/easing.js')}}"></script> 
<script type="text/javascript" src="{{asset('frontend/js/nav-hover.js')}}"></script>
<link href='{{asset('frontend/http://fonts.googleapis.com/css?family=Monda')}}' rel='stylesheet' type='text/css'>
<link href='{{asset('frontend/http://fonts.googleapis.com/css?family=Doppio+One')}}' rel='stylesheet' type='text/css'>
<script type="text/javascript">
  $(document).ready(function($){
    $('#dc_mega-menu-orange').dcMegaMenu({rowItems:'4',speed:'fast',effect:'fade'});
  });
</script>
</head>
<body>
 	<div class="wrap">
		@include('frontend.layout.headertop')
<!-- --------------------------------------- -->
        @include('frontend.layout.menu')
<!-- ---------------------------------------------- -->
        @if (str_contains(Request::route()->getName(), 'home'))
        @include('frontend.layout.headerbottom')
        @endif
        
<!-- ---------------------------------------------- -->
		@yield('content')
	</div>
<!-- --------------------------------------------- -->
   
        @include('frontend.layout.footer')

<!-- --------------------------------------------- -->      


    <script type="text/javascript">
		$(document).ready(function() {
			/*
			var defaults = {
	  			containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
	 		};
			*/
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
    <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
    <link href="{{asset('frontend/css/flexslider.css')}}" rel='stylesheet' type='text/css' />
	<script defer src="{{asset('frontend/js/jquery.flexslider.js')}}"></script>
	<script type="text/javascript">
	$(function(){
		SyntaxHighlighter.all();
	});
	$(window).load(function(){
		$('.flexslider').flexslider({
		animation: "slide",
		start: function(slider){
			$('body').removeClass('loading');
		}
		});
	});
	</script>
</body>
</html>
