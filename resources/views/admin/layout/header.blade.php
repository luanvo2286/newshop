<div class="grid_12 header-repeat">
            <div id="branding">
                <div class="floatleft logo">
                    <img src="{{asset('admin/img/livelogo.png')}}" alt="Logo" />
				</div>
				<div class="floatleft middle">
					<h1>Training with live project</h1>
					<p>www.trainingwithliveproject.com</p>
				</div>
                <div class="floatright">
                    <div class="floatleft">
                        <img src="{{asset('admin/img/img-profile.jpg')}}" alt="Profile Pic" /></div>
                    <div class="floatleft marginleft10">
                        <ul class="inline-ul floatleft">
                            <li>Hello Admin</li>
                            <li><a href="{{ route('admin.logout') }}">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="{{route('admin.home')}}"><span>Dashboard</span></a> </li>
                <li class="ic-form-style"><a href=""><span>User Profile</span></a></li>
				<li class="ic-typography"><a href="{{route('admin.changepassword')}}"><span>Change Password</span></a></li>
				<li class="ic-grid-tables"><a href="{{route('admin.inbox')}}"><span>Inbox</span></a></li>
                <li class="ic-charts"><a href="{{route('frontend.home')}}"><span>Visit Website</span></a></li>
            </ul>
        </div>
        <div class="clear">
</div>