
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    {{-- <meta name="csrf-token" content="{{ csrf_token() }}" /> --}}
    <title>Admin</title>
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/reset.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/text.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/grid.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/layout.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/nav.css')}}" media="screen" />
    <link href="{{asset('admin/css/table/demo_page.css')}}" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="{{asset('admin/js/jquery-1.6.4.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('admin/js/jquery-ui/jquery.ui.core.min.js')}}"></script>
    <script src="{{asset('admin/js/jquery-ui/jquery.ui.widget.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/js/jquery-ui/jquery.ui.accordion.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/js/jquery-ui/jquery.effects.core.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/js/jquery-ui/jquery.effects.slide.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/js/jquery-ui/jquery.ui.mouse.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/js/jquery-ui/jquery.ui.sortable.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/js/table/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="{{asset('admin/js/table/table.js')}}"></script>
    <script src="{{asset('admin/js/setup.js')}}" type="text/javascript"></script>
	 <script type="text/javascript">
        $(document).ready(function () {
            setupLeftMenu();
		    setSidebarHeight();
            
        });
    </script>

</head>
<body>
<div class="container_12">
    
        @include('admin.layout.header')

        @include('admin.layout.slider')

        @yield('content')
    <div class="clear">
    </div>
</div>
    
</body>
    @yield('script')
</html>
