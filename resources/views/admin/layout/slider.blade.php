<div class="grid_2">
            <div class="box sidemenu">
                <div class="block" id="section-menu">
                    <ul class="section menu">
                        <li><a class="menuitem">Site Option</a>
                            <ul class="submenu site-option">
                                <li><a href="{{route('admin.titleslogan')}}">Title & Slogan</a></li>
                                <li><a href="{{route('admin.social')}}">Social Media</a></li>
                                <li><a href="{{route('admin.copyright')}}">Copyright</a></li>
                            </ul>
                        </li>
                        
                        <li><a class="menuitem update-page">Update Pages</a>
                            <ul class="submenu">
                                <li><a>About Us</a></li>
                                <li><a>Contact Us</a></li>
                            </ul>
                        </li>
                        <li><a class="menuitem slider-option">Slider Option</a>
                            <ul class="submenu">
                                <li><a href="{{route('admin.slideradd')}}">Add Slider</a> </li>
                                <li><a href="{{route('admin.sliderlist')}}">Slider List</a> </li>
                            </ul>
                        </li>
                        <li><a class="menuitem category-option">Category Option</a>
                            <ul class="submenu">
                                <li><a href="{{route('admin.catadd')}}">Add Category</a> </li>
                                <li><a href="{{route('admin.catlist')}}">Category List</a> </li>
                            </ul>
                        </li>
                        <li><a class="menuitem product-option">Product Option</a>
                            <ul class="submenu">
                                <li><a href="{{route('admin.productadd')}}">Add Product</a> </li>
                                <li><a href="{{route('admin.productlist')}}">Product List</a> </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>