﻿@extends('admin.layout.app')
@section('content')
<div class="grid_10">
    <div class="box round first grid">
        <h2>Change Password</h2>
        <div class="block">               
         <form action="" method="post">
            @csrf
            <table class="form">					
                <tr>
                    <td>
                        <label>Old Password</label>
                    </td>
                    <td>
                        <input type="password" placeholder="Enter Old Password..."  name="oldpwd" class="medium" />
                    </td>
                </tr>
				 <tr>
                    <td>
                        <label>New Password</label>
                    </td>
                    <td>
                        <input type="password" placeholder="Enter New Password..." name="newpwd" class="medium" />
                    </td>
                </tr>
				 
				
				 <tr>
                    <td>
                        
                    </td>
                    <td>
                        <input type="submit" name="submit" Value="Update" />
                    </td>
                    
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div>
                            @if (\Session::has('changePass'))
                                <div>
                                    <ul>
                                        <li>{!! \Session::get('changePass') !!}</li>
                                    </ul>
                                </div>
                            @endif
                            @if($errors->any())
                                <div>
                                    <ul>
                                        @foreach($errors as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                {{-- <h4>{{$errors->first()}}</h4> --}}
                            @endif
                        </div>
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</div>
@endsection