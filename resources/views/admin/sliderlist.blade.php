@extends('admin.layout.app')

@section('content')
<div class="grid_10">
    <div class="box round first grid">
        <h2>Slider List</h2>
        <div class="block">  
            <table class="data display datatable" id="example">
			<thead>
				<tr>
					<th>No.</th>
					<th>Slider Title</th>
					<th>Slider Image</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@if (!empty($data))
					@foreach ($data as $item)
					<tr class="odd gradeX">
						<td>{{ $item['id'] }}</td>
						<td>{{ $item['title'] }}</td>
						<td><img src="{{ asset('upload/'.$item['image']) }}" height="40px" width="60px"/></td>				
						<td>
							<a >Edit</a> || 
							<a class="delete">Delete</a> 
						</td>
					</tr>
					@endforeach	
				@else
					<tr>
						<td colspan="4"> 
							<h4>Nothing to show!</span>
						</td>
					</tr>
				@endif
				
			</tbody>
		</table>

       </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();
        $('.datatable').dataTable();
		setSidebarHeight();

		
    });
</script>
@endsection


@section('script')
<script type="text/javascript">
	$(document).ready(function () {

		//edit slider here.



		$('.delete').click(function(e){
			e.preventDefault();

			const id = $(this).closest('.gradeX').find('td:nth-child(1)').text();
			
			var result = confirm('Are you sure to Delete!')
			if(result){
				console.log('ok');
				//write ajax here
				// $.ajaxSetup({
				// 	headers: {
				// 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				// 	}
				// });
				$.ajax({
					type: 'get',
					url: 'slider-delete/' + id,
					dataType: 'json',
					success: function(res){
						console.log(res.data);
					}
				})
			}
		})
		
		
	});
</script>
@endsection
