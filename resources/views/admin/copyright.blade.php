﻿@extends('admin.layout.app')
@section('content')
<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Copyright Text</h2>
        <div class="block copyblock"> 
         <form method="post" action="">
            @csrf
            <table class="form">					
                <tr>
                    <td>
                        <input type="text" placeholder="Enter Copyright Text..." name="copyright" class="large" />
                    </td>
                </tr>
				
				 <tr> 
                    <td>
                        <input type="submit" name="submit" Value="Update" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div>
                            @if (\Session::has('success'))
                                <div>
                                    <ul>
                                        <li>{!! \Session::get('success') !!}</li>
                                    </ul>
                                </div>
                            @endif
                            @if($errors->any())
                                <h4>{{$errors->first()}}</h4>
                           
                            @endif
                        </div>
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</div>
@endsection